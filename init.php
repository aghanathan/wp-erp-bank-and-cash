<?php
/*
Plugin Name: WP ERP - Bank & Cash
Plugin URI: https://wordpress.org/plugins/bank-cash
Description: Manage and display your Cash Books (Balances, Cash Receipts Book, Cash Payments Book) & Bank Accounts
Version: 0.1.0
Author: nath4n
Author URI: http://wedevs.com
Text Domain: erp-bankcash
Domain Path: /languages
License: GPL2
*/

add_action('admin_menu', 'createBNCMenus');

function createBNCMenus() {
	add_menu_page('ERP - Bank & Cash', 'ERP - Bank & Cash', 'manage_options', 'erp-bankcash-menu-slug', 'BankCashPageFunction', 'dashicons-welcome-widgets-menus', 90);
    add_submenu_page('erp-bankcash-menu-slug', 'Cash Receipts Book', 'Cash Receipts Book', 0, 'erp-bankcashreceipt-submenu-slug', 'BankCashReceiptFunction');
    add_submenu_page('erp-bankcash-menu-slug', 'Cash Payments Book', 'Cash Payments Book', 0, 'erp-bankcashpayment-submenu-slug', 'BankCashPaymentFunction');
}

function BankCashPageFunction() {
	echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
    echo '<h2>Cash Balances Book</h2>';
    echo '</div>';
}

function BankCashReceiptFunction() {
    echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
    echo '<h2>Cash Receipts Book</h2>';
    echo '</div>';
}

function BankCashPaymentFunction() {
    echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
    echo '<h2>Cash Payments Book</h2>';
    echo '</div>';
}